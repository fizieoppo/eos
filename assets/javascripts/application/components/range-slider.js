$(function () {
  $('.js-example-inner-box').on('click', '#slider', function () {
    const value = $('#slider').val()
    const fill = $(this).closest('.slider-container').find('.fill')
    fill.css('width', `${value}%`)
  })
})
