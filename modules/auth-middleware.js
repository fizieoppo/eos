const axios = require('axios');

const authMiddleware = async (req, res, next) => {
  if (!process.env.EOS_AUTH) return next()

  try {
    return await axios.get(`${process.env.EOS_STRAPI_SERVER_DEV}/users/me`, {
      headers: {
        Authorization: `Bearer ${req.cookies.token}`,
      }
    })
      .then(data => {
        next()
      })
  } catch (error) {
    if (req.originalUrl !== '/auth') {
      return req.originalUrl === '/api/auth'
        ? next()
        : res.redirect('/auth')
    } else {
      next()
    }
  }
}

module.exports = {
  authMiddleware
}
