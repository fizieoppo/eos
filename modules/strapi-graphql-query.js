const axios = require('axios')
const fs = require('fs')

/* Read the data from the config file */
const readConfigFile = async () => {
  try {
    const readFile = fs.readFileSync('config.json', (err, data) => {
      if (err) return console.log('ERROR => readConfigFile(): ', err)

      return data
    })

    return await JSON.parse(readFile)
  } catch (error) {
    console.log('readConfigFile(): ', error);
  }
}

const strapiGraphql = async query => {
  /* Get the data from config file */
  const readConfiFile = await readConfigFile()

  /* Destructuring the object */
  const { useStrapiAs, envVars: { dev, prod } } = readConfiFile

  const strapiRequestConfig = {
    url: useStrapiAs === 'dev' ? dev.strapiUrl : prod.strapiUrl,
    user: useStrapiAs === 'dev' ? dev.user : prod.user,
    pass: useStrapiAs === 'dev' ? dev.pass : prod.pass
  }

  try {
    const { url } = strapiRequestConfig
    const { data } = await axios.get(`${url}/graphql?query={${query}}`)

    return data
  } catch (error) {
    console.log('strapiGraphql(): ', error);
  }
}

module.exports = {
  strapiGraphql,
  readConfigFile
}
