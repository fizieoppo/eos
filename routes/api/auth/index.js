const express = require('express')
const router = express.Router()
const axios = require('axios')

/* Get JWT from Strapi  */
router.post('/', async (req, res) => {
  const { email, password } = req.body.user

  try {
    const { data } = await axios.post(`${process.env.EOS_STRAPI_SERVER_DEV}/auth/local`, {
      identifier: email,
      password
    })

    return res.json(data.jwt)
  } catch (err) {
    console.log(err)
  }
})

module.exports = router
