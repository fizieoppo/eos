const express = require('express')
const router = express.Router()
const { exec } = require('child_process')
const cloudinary = require('cloudinary')
const { zip } = require('zip-a-folder')

/* Config for Cloudinary API. More about credentials at: https://cloudinary.com/documentation/how_to_integrate_cloudinary#1_create_and_set_up_your_account */
cloudinary.config({
  cloud_name: process.env.EOS_CDN_NAME,
  api_key: process.env.EOS_CDN_API_KEY,
  api_secret: process.env.EOS_CDN_API_SEC
})

/* Compress dump folder */
class ZipAFolder {
  static async main () {
    await zip('./backup/dump', './backup/dump.zip')
  }
}

router.get('/', async (req, res, next) => {
  /* Execture mongodump command */
  exec(`mongodump --uri ${process.env.EOS_DATABASE_BACKUP_URI} -o ./backup/dump`, async (err, data) => {
    if (err) return res.send(err)
    try {
      /* Compress the dump folder into a zip file */
      await ZipAFolder.main()

      /* Upload the ZIP file to Cloudinary */
      const uploadFile = await cloudinary.v2.uploader.upload('./backup/dump.zip',
        {
          resource_type: 'raw',
          folder: 'DB'
        })

      /* Destructure the public+id and created at from the response */
      // eslint-disable-next-line
      const { public_id, created_at } = await uploadFile

      res.status(200).json({
        public_id,
        created_at
      })
    } catch (error) {
      res.status(400).send(error)
    }
  })
})

module.exports = router
