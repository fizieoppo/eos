// these variables need to be defined in the global scope
// so renderElementsStates can access them instead of the ones defined in
// the controller

const getElementsStatesService = sinon.fake()

describe("States implementation", () => {

  describe('should have different states for elements ', () => {
    const stateWrap = $('<ul class="js-states-list"><li class="js-state-current"><input class="js-element-id" type="radio" name="text"><label class="js-element-label"></label></li></ul><input class="js-form-element-state" type="text"><p class="js-how-to-use"></p><div class="js-example-inner-box"></div><pre class="js-snippet-code"><code></code></pre>')
    const collection = [{"type":"text","states":[{"status":"default","how_to_use":"default state","code":["<label>default</label>"]},{"status":"focused","how_to_use":"focused state","code":["<label>focused</label>"]},{"status":"disabled","how_to_use":"disabled state.","code":["<label>disabled</label>"]}]}]
    
    before( (done) => {
      $('body').prepend(stateWrap)
      done()
    })

    after( (done) => {
      $(stateWrap).remove()
      done()
    })

    it('Should print the states of element for input type', () => {
      $eleDisplayTemplate = $('.js-state-current').clone(true)
      $('.js-state-current').remove()
      
      const type = $('.js-form-element-state').attr('type')
      renderElementsStates(collection, type)

      expect($('.js-form-element-state').attr('type')).to.contain('text')
      expect($('.js-state-current')).to.have.lengthOf(3)
      expect($('.js-element-label')).to.contain('Default')
      expect($('.js-element-label')).to.contain('Focused')
      expect($('.js-element-label')).to.contain('Disabled')
    })

    it('Should print the how to use and code snippet text of selected state ', () => {
      const elementCollection = collection.filter(ele => ele.type === 'text').map(ele => ele.states)
      updateElementState(elementCollection[0], 'default')

      expect($('.js-how-to-use')).to.contain('default state')
      expect($('.js-snippet-code code')).to.contain('<label>default</label>')
    })
  })

})
